package com.cidadao.service;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;

import org.eclipse.microprofile.rest.client.inject.RestClient;

import com.cidadao.entity.Agendamento;
import com.cidadao.entity.DadosPessoais;
import com.cidadao.entity.Endereco;


@ApplicationScoped
public class AgendamentoService {
    Logger logger = Logger.getLogger(AgendamentoService.class.getName());
    @RestClient
    DadosPessoaisService dadosPessoaisService;

    static Map<String,Agendamento> agendamentos = new HashMap<String,Agendamento>();
    public Agendamento doRecuperarAgendamentoPorCpf(String cpf){
        Agendamento agendamento = new Agendamento();
        DadosPessoais dadosPessoais = dadosPessoaisService.doRecuperarInformacaoPessoalPorCpf(cpf);
        agendamento.dadosPessoias=dadosPessoais;
        return agendamento;
    }

    @PostConstruct
    private void mountAgendamento(){
        for(int i=0;i<=100;i++){
            Agendamento agendamento = new Agendamento();
            DadosPessoais dadosPessoais = getDadosPessoais(i);
            agendamento.dadosPessoias=dadosPessoais;
            agendamentos.put(dadosPessoais.cpf, agendamento);
        }
    }

    private DadosPessoais getDadosPessoais(int sequencia){
        NumberFormat df = new DecimalFormat("00000000000");
        String cpf = df.format(sequencia);
        logger.info("Indo buscar sequencia "+sequencia);
        DadosPessoais dadosPessoais = dadosPessoaisService.doRecuperarInformacaoPessoalPorCpf(cpf);
        
        return dadosPessoais;
    }

    
    public List<Agendamento> doRecuperarAgendamentos() {
        List<Agendamento> _agendamentos = new ArrayList<Agendamento>();
        _agendamentos.addAll(agendamentos.values());
        return _agendamentos;
    }
}
