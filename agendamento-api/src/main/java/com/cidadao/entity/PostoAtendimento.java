package com.cidadao.entity;

import java.util.List;

public class PostoAtendimento {
    public String nome;
    public List<DataHoraDisponiveis> dataHoraDisponiveis;
    public Endereco endereco;
}
