package com.cidadao;

import java.util.List;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.cidadao.entity.Agendamento;
import com.cidadao.service.AgendamentoService;

@Path("/agendamento")
public class AgendamentoResource {
    Logger logger = Logger.getLogger(AgendamentoResource.class.getName());
    @Inject
    AgendamentoService agendamentoService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response doRecuperarOpcoesAgendamento(
        @QueryParam("cpf") String cpf
    ) {
        logger.info("Recebendo Requiscao com cpf ["+cpf+"]:q!");
        if(cpf == null){
            List<Agendamento> agendamentos = agendamentoService.doRecuperarAgendamentos();
            return Response.ok(agendamentos).build();
        }else{
 
            Agendamento agendamento = agendamentoService.doRecuperarAgendamentoPorCpf(cpf);
            return Response.ok(agendamento).build();
        }
        
        
    }

   
}