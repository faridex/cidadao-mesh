package com.cidadao;

import java.util.List;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.cidadao.entity.DadosPessoais;
import com.cidadao.service.DadosPessoaisService;

@Path("/dados-pessoais")
public class DadosPessoaisResource {
    
    Logger logger = Logger.getLogger(DadosPessoaisResource.class.getName());

    @Inject
    DadosPessoaisService agendamentoService;

    @GET
    @Path("/filtros")
    @Produces(MediaType.APPLICATION_JSON)
    public DadosPessoais doRecuperarInformacaoPessoalPorCpf(
        @QueryParam("cpf") String cpf
    ) {
        logger.info("recebendo requisicao do filtro cpf "+cpf);
        DadosPessoais dados = agendamentoService.doRecuperarDadosPessoiasPorCpf(cpf);
        return dados;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public  List<DadosPessoais> doGetDadosPessoais() {
        List<DadosPessoais> dados = agendamentoService.doRecuperarDadosPessoais();
        return dados;
    }
}