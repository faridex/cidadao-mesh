package com.cidadao.entity;

public class Endereco {
    public Long versao;
    public String cep;
    public String cidade;
    public String bairro;
    public String uf;
    public String logradouro;
    public String complemento;
}
