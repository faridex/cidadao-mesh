package com.cidadao.service;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;

import org.eclipse.microprofile.config.inject.ConfigProperty;

import com.cidadao.entity.Endereco;


@ApplicationScoped
public class BuscarEnderecoService {
    Logger logger = Logger.getLogger(BuscarEnderecoService.class.getName());
    
    @ConfigProperty(name = "endereco.abortar.consulta") 
    Boolean abortarConsulta;

    @ConfigProperty(name = "endereco.delay.consulta") 
    Long delayConsulta;

    @ConfigProperty(name = "endereco.versao") 
    Long versao;

    
    
    static Map<String,Endereco> enderecos = new HashMap<String,Endereco>();
    public Endereco doRecuperarEnderecoCep(String cep){
        logger.info("Consultando Endereço...");
        if(abortarConsulta != null && abortarConsulta){
            logger.info("Abortando....");
            throw new RuntimeException();
        }else if(delayConsulta != null && delayConsulta.longValue() >0){
            try {
                logger.info("Aplicando Delay de "+ delayConsulta.longValue());
                Thread.sleep(delayConsulta);
            } catch (Exception e) {
                throw new RuntimeException();
            } 
        }
        Endereco endereco = enderecos.get(cep);
        if(endereco != null)
            logger.info("Endereco encontrado para o CEP[" + endereco.cep +"]");
        
        return endereco;
        


            

       
    }


    @PostConstruct
    private void mountAgendamento(){
        for(int i=0;i<100;i++){
            Endereco endereco = getEndereco(i);
            enderecos.put(endereco.cep, endereco);
        }
    }

    
    private Endereco getEndereco(int sequencia){
        NumberFormat df = new DecimalFormat("00000000");
        Endereco endereco = new Endereco();
        endereco.cep=df.format(sequencia);
        if(sequencia %2==0){
            endereco.logradouro="Avenida vai que vai "+sequencia;
        }else{
            endereco.logradouro="Rua bem Ali número "+sequencia;
        }
        endereco.bairro="Algum bairro do lado v"+sequencia;
        endereco.cidade="Cidade do Teste "+sequencia;
        endereco.uf="YU";
        endereco.complemento="Cond. du tatu";
        endereco.versao=versao;
        return endereco;
    }
    
    public List<Endereco> doRecuperarEnderecos() {
        List<Endereco> _enderecos = new ArrayList<Endereco>();
        _enderecos.addAll(enderecos.values());
        return _enderecos;
    }
}
