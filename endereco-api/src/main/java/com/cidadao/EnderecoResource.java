package com.cidadao;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import com.cidadao.entity.Endereco;
import com.cidadao.service.BuscarEnderecoService;

@Path("/endereco") 
public class EnderecoResource {

    @Inject
    BuscarEnderecoService enderecoService; 

    @GET
    @Path("/filtros")
    @Produces(MediaType.APPLICATION_JSON)
    public Endereco doRecuperarEnderecoPorCep(
        @QueryParam("cep") String cep
    ) {
        Endereco endereco = enderecoService.doRecuperarEnderecoCep(cep);
        return endereco;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public  List<Endereco> doGetDadosPessoais() {
        List<Endereco> enderecos = enderecoService.doRecuperarEnderecos();
        return enderecos;
    }
}