### Loop para simular requisições 
while true; do curl "http://istio-ingressgateway-istio-system.apps.cluster-k4629.k4629.sandbox2984.opentlc.com/agendamento?cpf=00000000022"; sleep 1; done

## mostrar tempo de resposta
curl -w 'Tempo %{time_total}' "http://istio-ingressgateway-istio-system.apps.cluster-k4629.k4629.sandbox2984.opentlc.com/agendamento?cpf=00000000022"

## Build para em ambiente M1,  força imagem base compativo  com amd64
docker build --platform linux/amd64 -f ./src/main/docker/Dockerfile.jvm -t quay.io/farid_ahmad/cidadao/endereco-api:1.0 .

## push to respository
docker push quay.io/farid_ahmad/cidadao/endereco-api:1.0

## Aplicar deploy agenda-api-vi
oc create/replace -f deployment-agenda-api-v1.yaml

## Aplicar deploy dados-pessoais-api-vi
oc create/replace -f deployment-dados-pessoais-api-v1.yaml

## Aplicar deploy endereco-api-vi
oc create/replace -f deployment-endereco-api-v1.yaml




